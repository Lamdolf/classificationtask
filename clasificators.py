from pandas import DataFrame

# Clasifiers
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.naive_bayes import BernoulliNB
from sklearn.preprocessing import normalize
from sklearn.ensemble import RandomForestClassifier

# Metrics for testing performance
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.model_selection import cross_validate


def getMetrics(clf, x, y):

    prediction = clf.predict(x)
    accuracy = accuracy_score(y_true=y,
                              y_pred=prediction)
    recall = recall_score(y_true=y,
                          y_pred=prediction, average="macro")
    precision = precision_score(y_true=y,
                                y_pred=prediction, average="macro")
    confusionMatrix = confusion_matrix(y_true=y,
                                       y_pred=prediction)

    print("\n")
    print(confusionMatrix)
    return {"Accuracy": accuracy,
            "Recall": recall, "Precision": precision}
        
    

def crossValidation(model, modelname, atributes, target):

    print("+---------------------------------------+")
    print(f"     Resultados para {modelname}")
    print("+---------------------------------------+")
    result = cross_validate(model, atributes, target, cv=5,
                            scoring=getMetrics, n_jobs=1,
                            return_train_score=True)
    print(f"Train Accuracy: {result['train_Accuracy']}")
    print(f"Test Accuracy: {result['test_Accuracy']}")
    print(f"Train Recall: {result['train_Recall']}")
    print(f"Test Recall: {result['test_Recall']}")
    print(f"Train Precision: {result['train_Precision']}")
    print(f"Test Precision: {result['test_Precision']}")


def normalizeData(data):

    subset = data.copy()
    cols = [x for x in subset.columns]
    target = subset["Star_type"].copy()
    cualitatives = subset[["Star_Color", "Spectral_class"]].copy()
    subset.drop(["Star_type", "Star_Color", "Spectral_class"],
                axis=1, inplace=True)
    
    normalized = normalize(subset.values)
    normalizedDf = DataFrame(normalized)
    normalizedDf.columns = cols[:4] 
    normalizedDf["Star_Color"] = cualitatives["Star_Color"].copy()
    normalizedDf["Spectral_class"] = cualitatives["Spectral_class"].copy()
    normalizedDf["Star_type"] = target.copy()

    return normalizedDf

    

def kNN(data):

    cleanData = normalizeData(data)
    dataArray = cleanData.values
    atributes, target = dataArray[:, :-1], dataArray[:,-1]

    for i in range(1,11):
        neigh = KNeighborsClassifier(n_neighbors=i)
        crossValidation(neigh, f"KNN with K={i}", atributes, target)


def decissionTree(data):

    dataArray = data.values
    atributes, target = dataArray[:, :-1], dataArray[:,-1]

    treeClf = DecisionTreeClassifier(criterion="entropy",
                                     splitter="best")

    crossValidation(treeClf, "Decision Tree (no prune)", atributes, target)
    for i in [(0+x*0.005) for x in range(1,11)]:
        treeClf = DecisionTreeClassifier(criterion="entropy",
                                         splitter="best",
                                         ccp_alpha=i)
        crossValidation(treeClf, f"Decision Tree (prune with alpha={i})",
                        atributes, target)


def naiveBayes(data):

    dataArray = data.values
    atributes, target = dataArray[:, :-1], dataArray[:,-1]

    gaussNB = GaussianNB()
    bernouNb = BernoulliNB()

    crossValidation(gaussNB, "Gaussian Naive Bayes", atributes, target)
    crossValidation(bernouNb, "BernoulliNB Naive Bayes", atributes, target)


def randomForest(data):

    dataArray = data.values
    atributes, target = dataArray[:, :-1], dataArray[:,-1]

    for n_trees in [10, 20, 50, 100, 200, 500]:
        randomTree = RandomForestClassifier(n_estimators=n_trees,
                                            criterion="entropy",
                                            ccp_alpha=0.02)

        crossValidation(randomTree, f"Random forest with {n_trees} trees",
                        atributes, target)

if __name__ == "__main__":

    import pandas as pd
    import preprocessor

    data = pd.read_csv("./dataset.csv")
    data = preprocessor.dataCleaner(data)
    kNN(data)
    decissionTree(data)
    naiveBayes(data)
    randomForest(data)
