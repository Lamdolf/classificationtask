import pandas as pd
from pandas.core.frame import DataFrame
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.ensemble import IsolationForest

def visualizations(stars):

    fig, axes = plt.subplots(2, 4, figsize=(20,10), sharey=True)
    i=0
    j=0
    for col in stars.columns:
        sns.histplot(ax=axes[j, i%4], data=stars, x=col)
        i += 1
        j = 0 + int(i/4)
    fig.suptitle("Distribuciones de datos", size=14)
    plt.savefig("./images/Distributions.png")
    # plt.show()
    
    plt.figure()
    fig, axes = plt.subplots(2, 2, figsize=(15, 10), sharex=True)
    sns.boxplot(ax=axes[0,0], x="Star type", y="Temperature (K)", data=stars)
    sns.boxplot(ax=axes[0,1], x="Star type", y="Luminosity(L/Lo)", data=stars)
    sns.boxplot(ax=axes[1,0], x="Star type", y="Radius(R/Ro)", data=stars)
    sns.boxplot(ax=axes[1,1], x="Star type", y="Absolute magnitude(Mv)",
                data=stars)
    fig.suptitle("Variables Cuantitativas vs Clase de estrella", size=14)
    fig.savefig("./images/Boxplots.png")
    # plt.show()
    

def colorPlot(stars):

    plt.figure(figsize=(20,10))
    sns.countplot(x="Star type", hue="Star color", data=stars)
    plt.legend(bbox_to_anchor=(1, 1.05))
    plt.suptitle("Color según tipo de estrella", size="large")
    plt.savefig("./images/ColorPlot.png")

    plt.figure(figsize=(10,5))
    sns.countplot(x="Star type", hue="Spectral Class", data=stars)
    plt.legend(bbox_to_anchor=(1, 1.05))
    plt.suptitle("Clase espectral por tipo de estrella", size="large")
    plt.savefig("./images/SpectralPlot.png")


def dataSummary(stars):

    print(f"Filas y columnas: {stars.shape}\n")
    print("Atributos, presencia de null y tipo de dato:")
    stars.info()


def dataCleaner(data):

    subset = data.copy()
    colorsID = {}
    classID = {}
    colors = []
    starclasses = []
    asignation = 0
    for color in subset["Star color"]:
        if color not in colorsID:
           colorsID[color] = asignation
           asignation += 1
        colors.append(colorsID[color])

    asignation = 0
    for spclass in subset["Spectral Class"]:
        if spclass not in classID:
            classID[spclass] = asignation
            asignation += 1
        starclasses.append(classID[spclass])

    subset["Spectral_class"] = starclasses
    subset["Star_Color"] = colors

    subset.drop(["Star color", "Spectral Class"], axis=1, inplace=True)

    aux = subset["Star type"].copy()
    subset.drop(["Star type"], axis=1, inplace=True)
    subset["Star_type"] = aux

    return subset


if __name__ == "__main__":
    data = pd.read_csv("./dataset.csv")
    # dataSummary(data)
    # visualizations(data)
    # colorPlot(data)
    cleanData = dataCleaner(data)
    print(cleanData)
